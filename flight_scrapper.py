import time
import datetime

from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import WebDriverException
from chrome_driver import ChromeDriver


class FlightScrapper:
    """
    Class for scrapping data about flights from Emirates website
    """
    def __init__(self):
        """
        _options: get options for Chrome driver
        _service: install chrome driver
        _driver: get driver from custom class
        departure_airport, arrival_airport, flight_date: value which user input
        sleep_time: value for time.sleep() so that the site does not block the bot
        date_format: for correct work with date
        result_list: for saving final result
        """
        self._options = self._set_options()
        self._service = Service(ChromeDriverManager().install())
        self._driver = ChromeDriver(self._service, self._options)
        self.departure_airport = None
        self.arrival_airport = None
        self.flight_date = None
        self.sleep_time = 3
        self.date_format = '%d  %b %y'
        self.result_list = []

    def _set_options(self):
        """
        This method is return options for Chrome web driver
        """

        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--disable-blink-features=AutomationControlled')
        chrome_options.add_argument('--enable-experimental-cookie-features')
        return chrome_options

    def get_flight_detail(self):
        """
        This method ask users needed information for scrapping
        """
        self.departure_airport = str(input("Enter departure airport (example: London): "))
        self.arrival_airport = str(input("Enter arrival airport (example: Dubai): "))
        self.flight_date = datetime.datetime.strptime(
            str(input("Enter departing time (example: 31-10-2022): ")), '%d-%m-%Y')
        self.select_needed_flight_data()

    def select_needed_flight_data(self):
        """
        This method for opening Emirates webpage and input user entered values
        """
        try:
            with self._driver as driver:
                url = 'https://www.emirates.com/english/'
                driver.get(url)
                time.sleep(self.sleep_time)
                driver.find_element(By.ID, 'onetrust-accept-btn-handler').click()
                time.sleep(self.sleep_time)
                driver.find_element(By.NAME, 'Departure airport').clear()
                driver.find_element(By.NAME, 'Departure airport').send_keys(f'{self.departure_airport}')
                time.sleep(self.sleep_time)
                driver.find_element(By.CLASS_NAME, 'to-highlight').click()
                time.sleep(self.sleep_time)
                driver.find_element(By.NAME, 'Arrival airport').clear()
                driver.find_element(By.NAME, 'Arrival airport').send_keys(f'{self.arrival_airport}')
                time.sleep(self.sleep_time)
                driver.find_element(By.CLASS_NAME, 'highlight').click()
                time.sleep(self.sleep_time)
                driver.find_element(By.CLASS_NAME, 'js-one-way').click()
                time.sleep(self.sleep_time)
                driver.find_element(By.XPATH, f'//a[@aria-label="{self.change_flight_date_format()}"]').click()
                time.sleep(self.sleep_time)
                driver.find_element(By.XPATH,
                                    '//*[@id="panel0"]/div[2]/div/div/section/div[4]/div[2]/div[3]/form/button').click()
                time.sleep(25)
                self.get_data(BeautifulSoup(driver.page_source, 'html.parser'))
        except WebDriverException:
            raise ValueError("Invalid format of bad response!")

    def change_flight_date_format(self):
        """
        This method change date format
        """
        if self.flight_date is not None:
            return self.flight_date.strftime(self.date_format)

    def get_data(self, soup):
        """
        This method get data from page using BS4
        """
        for item in soup.select('div.ts-fbr-flight-list__body'):
            for i in item.select(
                    'div.ts-fbr-flight-list-row.flights-row.direct-flights-only.emirates-flights-only.EK_flight')[:3]:
                result = {}
                flight_time = i.select('time.ts-fie__departure')
                flight_cost = i.select('''
                     table.ts-fbr-brand-table__core-table.ts-fbr-brand-table__core-table--default
                     tbody div.ts-fbr-brand-table__price-container p.ts-fbr-brand-table__price-value
                     strong
                     ''')
                result['flight_time'] = flight_time[0].text[1:-1]
                result['flight_cost'] = flight_cost[0].text[1:]
                self.result_list.append(result)
        self.save_result(self.result_list)

    def save_result(self, data):
        """
        This method save data to result.json file
        """
        with open('result.json', 'a', encoding='utf-8') as file:
            file.write(str(data))


flight = FlightScrapper()
flight.get_flight_detail()
